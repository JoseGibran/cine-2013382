<?php
/**
 * Created by PhpStorm.
 * User: gibran
 * Date: 11/07/15
 * Time: 09:24 PM
 */

class Cine extends Eloquent{

    protected $table = 'Cine';

    public $timestamps = false;

    public function salas(){
        return $this->hasMany('Sala', 'cine_id');
    }

}
