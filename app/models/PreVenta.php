<?php
/**
 * Created by PhpStorm.
 * User: gibran
 * Date: 30/07/15
 * Time: 12:23 PM
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class PreVenta extends Eloquent {

    protected $table = 'PreVenta';
    public $timestamps = false;

    public function estreno(){
       return $this.belongsTo('Cine');
    }
}