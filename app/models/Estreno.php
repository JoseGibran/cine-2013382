<?php
/**
 * Created by PhpStorm.
 * User: gibran
 * Date: 16/07/15
 * Time: 08:50 PM
 */

class Estreno extends Eloquent{

    protected $table = 'Estreno';

    public $timestamps = false;


    public function pelicula(){
        return $this->hasOne('Pelicula');
    }
    public function preVenta(){
        return $this -> hasone('PreVenta');
    }
}