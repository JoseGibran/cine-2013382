<?php
/**
 * Created by PhpStorm.
 * User: gibran
 * Date: 14/07/15
 * Time: 10:00 AM
 */

class Cartelera extends Eloquent{

    protected $table = 'Cartelera';

    public $timestamps = false;

    public function sala(){
        return $this->belongsTo('Sala');
    }
    public function pelicula(){
        return $this->belongsTo('Pelicula');
    }

}