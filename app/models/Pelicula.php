<?php
/**
 * Created by PhpStorm.
 * User: gibran
 * Date: 10/07/15
 * Time: 05:01 PM
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class Pelicula extends Eloquent{

    protected $table = 'Pelicula';

    public $timestamps = false;

    public function preVenta(){
        return $this -> hasOne('PreVenta');
    }
}