<?php
/**
 * Created by PhpStorm.
 * User: gibran
 * Date: 14/07/15
 * Time: 09:49 AM
 */

class Sala extends Eloquent{

    protected $table='Sala';

    public $timestamps = false;

    public function cine(){
        return $this->belongsTo('Cine');
    }

    public function carteleras(){
        return $this -> hasMany('Cartelera', 'sala_id');
    }
}