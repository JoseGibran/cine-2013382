<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', function()
{
   return View::make('login/login', array('error' => false));
	//return View::make('start',  array('name' => 'Jose Gibran'));
});
Route::get('registro', function(){
    return View::make('login/registro');
});
Route::get('salir', function(){
    Auth::logout();
    return View::make('login/login', array('error' => false));
});
Route::get('/', function()
{
    return View::make('start');
});



Route::controller('check', 'LoginController');



Route::group(array('prefix' => 'peliculas'), function(){
    Route::controller('/', 'PeliculasController');
    Route::get('estrenos', 'PeliculasController@getEstrenos');
    Route::get('agregar', 'PeliculasController@getAgregar');
    Route::get('create', 'PeliculasController@getCreate');
    Route::get('delete/{id}', 'PeliculasController@getDelete');
    Route::get('editar/{id}', 'PeliculasController@getEditar');
    Route::get('update', 'PeliculasController@getUpdate');
    Route::get('detalles/{id}', 'PeliculasController@getDetalles');

});



Route::group(array('prefix' => 'cines'), function(){
    Route::controller('/', 'CinesController');
    Route::get('agregar', 'CinesController@getAgregar' );
    Route::get('create', 'CinesController@getCreate');
    Route::get('eliminar/{id}', 'CinesController@getEliminar');
    Route::get('editar/{id}', 'CinesController@getEditar');
    Route::get('edit', 'CinesController@getEdit');
});

Route::group(array('prefix' => 'salas'), function(){
    Route::controller('/', 'SalasController');
    Route::get('agregar', 'SalasController@getAgregar');
    Route::get('create', 'SalasController@getCreate');
    Route::get('eliminar/{id}','SalasController@getEliminar' );
    Route::get('editar/{id}', 'SalasController@getEditar');
    Route::get('edit', 'SalasController@getEdit');
});
Route::group(array('prefix' => 'carteleras'), function(){
    Route::controller('/', 'CartelerasController');
    Route::get('agregar', 'CartelerasController@getAgregar');
    Route::get('create', 'CartelerasController@getCreate');
    Route::get('eliminar/{id}', 'CartelerasController@getEliminar');
    Route::get('editar/{id}', 'CartelerasController@getEditar');
    Route::get('edit', 'CartelerasController@getEdit');
});

Route::group(array('prefix' => 'tipoSalas'), function(){
    Route::controller('/','TipoSalasController');
    Route::get('agregar', 'TipoSalasController@getAgregar');
    Route::get('create', 'TipoSalasController@getCreate');
    Route::get('eliminar/{id}', 'TipoSalasController@getEliminar');
    Route::get('editar/{id}', 'TipoSalasController@getEditar');
    Route::get('edit', 'TipoSalasController@getEdit');
});

Route::group(array('prefix' => 'formatoPeliculas'), function(){
    Route::controller('/', 'FormatoPeliculasController');
    Route::get('agregar', 'FormatoPeliculasController@getAgregar');
    Route::get('create', 'FormatoPeliculasController@getCreate');
    Route::get('eliminar/{id}', 'FormatoPeliculasController@getEliminar');
    Route::get('editar/{id}', 'FormatoPeliculasController@getEditar');
    Route::get('edit', 'FormatoPeliculasController@getEdit');
});


Route::get('detalleCine/{id}', 'CinesController@show');

Route::controller('plantillas', 'PlantillasController');



Route::group(array('prefix' => 'api'), function()
{
    Route::resource('preventas', 'PeliculasController@getPreventasJson');
    Route::resource('peliculas', 'PeliculasController');
    Route::resource('cines', 'CinesController');
    Route::resource('estrenos', 'PeliculasController@getEstrenosJson');
    Route::resource('detalleCine/{id}', 'CinesController@showJson');

});





