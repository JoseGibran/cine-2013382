@extends('layout.Plantilla')
@section('titulo')
    {{'Formato de Pelicula'}}
@endsection

@section('body')
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Descripcion
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($formatoPeliculas as $fp){?>
            <tr>
                <td>{{$fp -> nombre;}}</td>
                <td>{{$fp -> descripcion;}}</td>
                <td>
                    <a href="/Cine/public/formatoPeliculas/eliminar/{{$fp -> id}}"><button class="btn btn-danger">Eliminar</button></a>
                    <a href="/Cine/public/formatoPeliculas/editar/{{$fp -> id}}"><button class="btn btn-warning">Editar</button></a>
                </td>
            </tr>
            <?php }?>
            </tbody>
        </table>
        <a href="/Cine/public/formatoPeliculas/agregar"><button class="btn btn-success">Agregar</button></a>
    </div>
@endsection