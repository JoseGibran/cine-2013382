@extends('layout.Plantilla')

@section('titulo')
    {{'Editar Formato de Pelicula'}}
@endsection

@section('body')
    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Editar Formato de Pelicula</h3>
        <table class="table">
            <form action="/Cine/public/formatoPeliculas/edit" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="nombre">Nombre:</label>
                        </td>
                        <td>
                            <input type="hidden" name="id" value="{{$formatoPelicula -> id}}">
                            <input type='text' name="nombre" value="{{$formatoPelicula -> nombre}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="descripcion">Descripcion:</label>
                        </td>
                        <td>
                            <input type='text' name="descripcion" value="{{$formatoPelicula -> descripcion}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="Agregar" class="btn btn-success"/>

                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/formatoPeliculas"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>
            </tr>
        </table>
    </div>

@endsection