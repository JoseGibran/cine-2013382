@extends('layout.Plantilla')

@section('titulo')
    {{'Carteleras'}}
@endsection

@section('body')
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Sala
                    </th>
                    <th>
                        Pelicula
                    </th>
                    <th>
                        Formato
                    </th>
                    <th>
                        Lenguaje
                    </th>
                    <th>
                        Fecha
                    </th>
                    <th>
                        Hora
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($carteleras as $c){ ?>
                    <tr>
                        <td>
                            {{Sala::find($c->sala_id)->numero .' ' . Cine::find(Sala::find($c->sala_id) -> cine_id)->nombre}}
                        </td>
                        <td>
                            {{Pelicula::find($c->pelicula_id)->titulo}}
                        </td>
                        <td>
                            {{FormatoPelicula::find($c->formatopelicula_id)->nombre}}
                        </td>
                        <td>
                            {{$c -> formato_lenguaje}}
                        </td>
                        <td>
                            {{$c->fecha}}
                        </td>
                        <td>
                           {{$c -> hora}}
                        </td>
                        <td>
                            <a href="/Cine/public/carteleras/eliminar/{{$c -> id}}"><button class="btn btn-danger">Eliminar</button></a>
                            <a href="/Cine/public/carteleras/editar/{{$c -> id}}"><button class="btn btn-warning">Editar</button></a>
                        </td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <a href="/Cine/public/carteleras/agregar"><button class="btn btn-success">Agregar</button></a>
    </div>
@endsection