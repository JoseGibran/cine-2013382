@extends('layout.Plantilla')
@section('titulo')
    {{'Agregar Cartelera'}}
@endsection

@section('body')
    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Nueva Cartelera</h3>
        <table class="table">
            <form action="/Cine/public/carteleras/create" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="sala_id">Sala:</label>
                        </td>
                        <td>
                            <select name="sala_id">
                                <?php
                                foreach(Sala::all() as $s){?>
                                <option value="{{$s->id}}">{{$s -> numero .' ' . Cine::find($s -> cine_id)->nombre}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="pelicula_id">Pelicula:</label>
                        </td>
                        <td>
                            <select name="pelicula_id">
                                <?php
                                foreach(Pelicula::all() as $p){?>
                                <option value="{{$p->id}}">{{$p -> titulo}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="formatopelicula_id">Formato:</label>
                        </td>
                        <td>
                            <select name="formatopelicula_id">
                                <?php
                                foreach(FormatoPelicula::all() as $f){?>
                                <option value="{{$f->id}}">{{$f -> nombre}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="formato_lenguaje">Lenguaje:</label>
                        </td>
                        <td>
                            <input type='text' name="formato_lenguaje">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="fecha">Fecha:</label>
                        </td>
                        <td>
                            <input type='text' name="fecha">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="hora">Hora:</label>
                        </td>
                        <td>
                            <input type='text' name="hora">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="Agregar" class="btn btn-success"/>

                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/carteleras"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>
            </tr>
        </table>
    </div>

@endsection