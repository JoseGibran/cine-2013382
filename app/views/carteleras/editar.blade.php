@extends('layout.Plantilla')
@section('titulo')
    {{'Editar Cartelera'}}
@endsection

@section('body')
    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Editar Cartelera</h3>
        <table class="table">
            <form action="/Cine/public/carteleras/edit" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="sala_id">Sala:</label>
                        </td>
                        <td>
                            <input type="hidden" value="{{$cartelera -> id}}" name="id">
                            <select name="sala_id">
                                <?php
                                     $selected = ' ';
                                foreach(Sala::all() as $s){
                                    if($s -> id == $cartelera -> sala_id){
                                        $selected = 'selected';
                                    }else{
                                        $selected = ' ';
                                    }
                                ?>
                                <option value="{{$s->id}}" {{$selected}}>{{$s -> numero .' ' . Cine::find($s -> cine_id)->nombre}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="pelicula_id">Pelicula:</label>
                        </td>
                        <td>
                            <select name="pelicula_id">
                                <?php
                                    $selected=' ';
                                foreach(Pelicula::all() as $p){
                                    if($p -> id == $cartelera -> pelicula_id){
                                        $selected = 'selected';
                                    }else{
                                        $selected= ' ';
                                    }
                                ?>
                                <option value="{{$p->id}}" {{$selected}}>{{$p -> titulo}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="formatopelicula_id">Formato:</label>
                        </td>
                        <td>
                            <select name="formatopelicula_id">
                                <?php
                                    $selected= ' ';
                                foreach(FormatoPelicula::all() as $f){
                                    if($f -> id == $cartelera ->formatopelicula_id){
                                        $selected = 'selected';
                                    }else{
                                        $selected = ' ';
                                    }
                                ?>
                                <option value="{{$f->id}}" {{$selected}}>{{$f -> nombre}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="formato_lenguaje">Lenguaje:</label>
                        </td>
                        <td>
                            <input type='text' name="formato_lenguaje" value="{{$cartelera -> formato_lenguaje}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="fecha">Fecha:</label>
                        </td>
                        <td>
                            <input type='text' name="fecha" value="{{$cartelera -> fecha}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="hora">Hora:</label>
                        </td>
                        <td>
                            <input type='text' name="hora" value="{{$cartelera -> hora}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="editar" class="btn btn-success"/>

                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/carteleras"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>
            </tr>
        </table>
    </div>

@endsection