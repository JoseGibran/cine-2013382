<?php
if(Auth::check()){ ?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->

    @yield("css")

    <title>@yield('titulo')</title>

</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-static-top navbar-inverse" role="navigation">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button> <a class="navbar-brand" href="/Cine/public">Cine-2013382</a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administracion<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/Cine/public/peliculas">Peliculas</a>
                                </li>
                                <li>
                                    <a href="/Cine/public/cines">Cines</a>
                                </li>
                                <li>
                                    <a href="/Cine/public/salas">Salas</a>
                                </li>

                                <li>
                                    <a href="/Cine/public/peliculas/estrenos">Estrenos y Preventa</a>
                                </li>
                                <li>
                                    <a href="/Cine/public/carteleras">Cartelera</a>
                                </li>

                                <li class="divider">
                                </li>
                                <li>
                                    <a href="/Cine/public/tipoSalas">Tipo Sala</a>
                                </li>
                                <li>
                                    <a href="/Cine/public/formatoPeliculas">Formato Pelicula</a>
                                </li>
                                @yield('links')
                            </ul>
                        </li>
                    </ul>
                    @yield('search')
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">{{Auth::user()->userName}}</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configuracion<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/Cine/public/salir">Logout</a>
                                </li>

                                @yield('dropdown_links')
                            </ul>
                        </li>
                    </ul>
                </div>

            </nav>
        </div>
    </div>
    <div class="row">
        @yield('body')
    </div>
</div>

<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
@yield('scripts')
</body>
</html>


<?php
}else{
    echo '<meta http-equiv="Refresh" content=0;url="/Cine/public/login">';
}

?>

