@extends('layout.Plantilla')

@section('titulo')
    {{$pelicula -> titulo}}
@endsection

@section('css')
    <style>
        img{
            width: 40%;
            height: 40%;
        }
    </style>
@endsection

@section('body')
        <div class="col-md-3"></div>
        <div class="col-md-6 center-block">
            <img src="{{$pelicula -> image}}" style="display: inline-block">
            <div style="display:inline-block;">
                <p>
                <h2>{{$pelicula -> titulo}}</h2>
                    <span class="label label-success">{{$pelicula -> rated}}</span>
                    <span class="label label-primary">Duracion</span>
                    <span class="label label-danger">{{$pelicula -> genero}}</span>
                </p>

            </div>
            <h3>Sinopsis</h3>
            <p>
                {{$pelicula->sinopsis}}

            </p>
            <h3>Trailer</h3>
            <iframe class="youtube-player" type="text/html" width="640" height="385" src="http://www.youtube.com/embed/{{substr($pelicula->trailer_url, 24, strlen($pelicula->trailer_url))}}   " frameborder="0"></iframe>
        </div>

@endsection

