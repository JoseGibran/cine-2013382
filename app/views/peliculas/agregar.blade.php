@extends('layout.Plantilla')
@section('titulo')
    {{"Agregar Pelicula"}}
@endsection

@section('body')
    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Nueva Pelicula</h3>
        <table class="table">
            <form action="/Cine/public/peliculas/create" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="titulo">Titulo:</label>
                        </td>
                        <td>
                            <input type='text' name="titulo">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="sinopsis">Sinopsis:</label>
                        </td>
                        <td>
                            <input type='text' name="sinopsis">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="trailer_url">Trailer:</label>
                        </td>
                        <td>
                            <input type='text' name="trailer_url">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="image">Imagen:</label>
                        </td>
                        <td>
                            <input type='text' name="image">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="rated">Categoria:</label>
                        </td>
                        <td>
                            <input type='text' name="rated">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="genero">Genero:</label>
                        </td>
                        <td>
                            <input type='text' name="genero">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group" >
                        <td>
                            <label for="fecha_comienzo" >Fecha de Estreno:</label>
                        </td>
                        <td>
                            <input type="text" name="fecha_comienzo" id="inFecha_comienzo">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="fecha_fin">Fecha de Finalizacion:</label>
                        </td>
                        <td>
                            <input type="text" name="fecha_fin" id="inFecha_fin">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="cine_id">Cine:</label>
                        </td>
                        <td>
                            <select name="cine_id" id="selCine" disabled>
                                <?php
                                foreach(Cine::all() as $c){?>
                                <option value="{{$c->id}}">{{$c -> nombre}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group" >
                        <td>
                            <label for="isEstreno">Es estreno?</label>
                            <input type='checkbox' name="isEstreno" id="chkEstreno">
                        </td>
                        <td>
                            <label for="isPreVenta">Disponible Pre-Venta?</label>
                            <input type='checkbox' name="isPreVenta" id="chkPreVenta" disabled>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                    <td>

                    </td>
                    <td>
                        <input type='submit' id="submit" value="Agregar" class="btn btn-success"/>
                    </td>
                    </div>
                </tr>
            </form>
                <tr>
                    <td>

                    </td>
                    <td>
                        <a href="/Cine/public/peliculas"><Button class="btn btn-warning">Cancelar</Button></a>
                    </td>

                </tr>
        </table>
    </div>

@endSection
@section('scripts')
    <script>
        $(document).ready(function(){
            $("#chkEstreno").prop( "checked", false );
            $("#chkPreVenta").prop("checked", false);
            var estrenoPressed = false;
            var preVentaPressed = false;

            var $inFecha_comienzo = $('#inFecha_comienzo');
            var $inFecha_fin = $('#inFecha_fin');
            var $selCine = $('#selCine');

            $inFecha_comienzo.attr('disabled','disabled');
            $inFecha_fin.attr('disabled','disabled');
            $("#chkEstreno").click(function(){
                if(estrenoPressed == false){
                    $inFecha_comienzo.removeAttr('disabled');
                    $inFecha_fin.removeAttr('disabled');
                    $("#chkPreVenta").removeAttr('disabled');
                    estrenoPressed = true;

                }else{
                    $inFecha_comienzo.attr('disabled','disabled');
                    $inFecha_fin.attr('disabled','disabled');
                    $("#chkPreVenta").attr('disabled', 'disabled');
                    $selCine.attr('disabled', 'disabled');
                    $("#chkPreVenta").prop("checked", false);
                    preVentaPressed = false;
                    estrenoPressed = false;
                }
            });
            $("#chkPreVenta").click(function(){
                if(preVentaPressed == false){
                    $selCine.removeAttr('disabled')
                    preVentaPressed = true;
                }else{
                    $selCine.attr('disabled', 'disabled');
                    preVentaPressed = false;
                }
            });
        });
    </script>
@endsection