@extends('layout.Plantilla')
@section('titulo')
    {{$titulo}}
@endsection
@section('css')
    <style>
        .thumbnail img{
            width: 50%;
            height: 50%;
        }
    </style>
@endsection
@section('body')
    <div class="col-md-12">
        <div class="col-md-1 alert-success">Estrenos</div><br>
        <div class="col-md-1 alert-info">Pre-ventas</div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Genero
                    </th>
                    <th>
                        Clasificacion
                    </th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach($peliculas as $p){
                $estreno = DB::table('Estreno')
                        ->where('Estreno.pelicula_id', '=', $p -> id)
                        ->first();
                if(count($estreno) > 0){
                    $preVenta = DB::table('PreVenta')
                            ->where('PreVenta.estreno_id', '=', $estreno->id)
                            ->first();
                    if(count($estreno) > 0  && $estreno -> estado == 1){
                        echo "<tr class='alert-success'>";
                    }else{
                        echo "<tr>";
                    }
                    if(count($preVenta) > 0){
                        echo "<tr class='alert-info'>";
                    }
                }


            ?>
                    <td>{{$p -> titulo;}}</td>
                    <td>{{$p -> genero;}}</td>
                    <td>{{$p -> rated;}}</td>
                    <td>
                        <a href="/Cine/public/peliculas/delete/{{$p -> id}}"><button class="btn btn-danger">eliminar</button></a>
                        <a href="/Cine/public/peliculas/editar/{{$p -> id}}"><button class="btn btn-warning">editar</button></a>
                    </td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <a href="/Cine/public/peliculas/agregar"><button class = 'btn btn-success'>Agregar</button></a>

    </div>
@endsection