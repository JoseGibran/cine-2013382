@extends('layout.Plantilla')
@section('titulo')
    {{"Editar Pelicula"}}
@endsection

@section('body')
    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Editar Pelicula</h3>
        <table class="table">
            <form action="/Cine/public/peliculas/update" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="titulo">Titulo:</label>
                        </td>
                        <td>
                            <input type="hidden" name="id" value="{{$pelicula->id}}">
                            <input type='text' name="titulo" value="{{$pelicula->titulo}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="sinopsis">Sinopsis:</label>
                        </td>
                        <td>
                            <input type='text' name="sinopsis" value="{{$pelicula->sinopsis}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="trailer_url">Trailer:</label>
                        </td>
                        <td>
                            <input type='text' name="trailer_url" value="{{$pelicula->trailer_url}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="image">Imagen:</label>
                        </td>
                        <td>
                            <input type='text' name="image" value="{{$pelicula->image}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="rated">Categoria:</label>
                        </td>
                        <td>
                            <input type='text' name="rated" value="{{$pelicula->rated}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="genero">Genero:</label>
                        </td>
                        <td>
                            <input type='text' name="genero" value="{{$pelicula->genero}}">
                            <input type="hidden" name="agregar" value="true"/>
                        </td>
                    </div>
                </tr>
                <tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="Guardar" class="btn btn-info"/>

                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/peliculas"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>

            </tr>
        </table>
    </div>



@endsection