<!DOCTYPE html>
<html>
<head>
    <title>Registro</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 "></div>
                <div class="col-md-6 ">
                    <h3 class="panel panel-active">Registro</h3>
                    <table class="table">
                        <form action="check/registro" method='POST'>
                            <tr>
                                <div class="form-group">
                                    <td>
                                        <label for="userName">Nombre de Usuario:</label>
                                    </td>
                                    <td>
                                        <input type='text' name="userName">
                                    </td>
                                </div>
                            </tr>
                            <tr>
                                <div class="form-group">
                                    <td>
                                        <label for="password">Contraseña:</label>
                                    </td>
                                    <td>
                                        <input type='password' name="password">
                                    </td>
                                </div>
                            </tr>
                            <tr>
                                <div class="form-group">
                                    <td>

                                    </td>
                                    <td>
                                        <input type='submit' id="submit" value="Registrarse" class="btn btn-info"/>

                                    </td>
                                </div>
                            </tr>
                        </form>
                    </table>
                </div>
            </div>
        </div>

    </body>
</head>
</html>