@extends('layout.Plantilla')
@section('titulo')
    {{'salas'}}
@endsection
@section('body')
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th>
                    Numero
                </th>
                <th>
                    Cine
                </th>
                <th>
                    Tipo
                </th>
            </tr>
            </thead>

            <tbody>

            <?php
            foreach($salas as $s){?>
            <tr>
                <td>{{$s -> numero;}}</td>
                <td>{{Cine::find($s->cine_id)->nombre;}}</td>
                <td>{{TipoSala::find($s->tiposala_id)->nombre;}}</td>
                <td>
                    <a href="/Cine/public/salas/eliminar/{{$s -> id}}"><button class="btn btn-danger">Eliminar</button></a>
                    <a href="/Cine/public/salas/editar/{{$s -> id}}"><button class="btn btn-warning">Editar</button></a>
                </td>
            </tr>
            <?php }?>


            </tbody>

        </table>
        <a href="/Cine/public/salas/agregar"><button class="btn btn-success">Agregar</button></a>
    </div>
@endsection