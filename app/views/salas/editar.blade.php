@extends('layout.Plantilla')
@section('title')
    {{'Editar Sala'}}}
@endsection

@section('body')

    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Editar sala {{$sala -> id}}</h3>
        <table class="table">
            <form action="/Cine/public/salas/create" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="numero">Numero Sala:</label>
                        </td>
                        <td>
                            <input type="hidden" name="id" value="{{$sala -> id}}">
                            <input type='number' name="numero" value="{{$sala -> numero}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="cine_id">Cine:</label>
                        </td>
                        <td>
                            <select name="cine_id">
                                <?php
                                    $selected = ' ';
                                foreach(Cine::all() as $c){
                                    if($sala -> cine_id == $c ->id){
                                        $selected = 'selected';
                                    }else{
                                        $selected = ' ';
                                    }
                                ?>
                                <option value="{{$c->id}}" {{$selected}}>{{$c -> nombre}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="tiposala_id">Tipo De Sala:</label>
                        </td>
                        <td>
                            <select name="tiposala_id">
                                <?php
                                    $selected = ' ';
                                    foreach(TipoSala::all() as $tipo){
                                    if($sala -> tiposala_id == $tipo -> id){
                                        $selected = ' selected';
                                    }else{
                                        $selected = ' ';
                                    }
                                ?>
                                <option value="{{$tipo -> id}}">{{$tipo -> nombre}}</option>
                                <?php }?>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="Agregar" class="btn btn-success"/>

                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/salas"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>

            </tr>
        </table>
    </div>

@endsection