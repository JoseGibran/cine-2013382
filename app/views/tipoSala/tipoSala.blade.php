@extends('layout.Plantilla')
@section('titulo')
    {{'Tipos de Sala'}}
@endsection

@section('body')
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Descripcion
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($tipoSalas as $ts){?>
            <tr>
                <td>{{$ts -> nombre;}}</td>
                <td>{{$ts -> descripcion;}}</td>
                <td>
                    <a href="/Cine/public/tipoSalas/eliminar/{{$ts -> id}}"><button class="btn btn-danger">Eliminar</button></a>
                    <a href="/Cine/public/tipoSalas/editar/{{$ts -> id}}"><button class="btn btn-warning">Editar</button></a>
                </td>
            </tr>
            <?php }?>
            </tbody>
        </table>
        <a href="/Cine/public/tipoSalas/agregar"><button class="btn btn-success">Agregar</button></a>
    </div>
@endsection