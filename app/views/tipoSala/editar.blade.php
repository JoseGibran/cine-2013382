@extends('layout.Plantilla')

@section('titulo')
    {{'Editar Tipo de Sala'}}
@endsection

@section('body')

    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Nuevo Tipo de Sala</h3>
        <table class="table">
            <form action="/Cine/public/tipoSalas/edit" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="nombre">Nombre:</label>
                        </td>
                        <td>
                            <input type="hidden" name="id" value="{{$tipoSala -> id}}">
                            <input type='text' name="nombre" value="{{$tipoSala -> nombre}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="descripcion">Descripcion:</label>
                        </td>
                        <td>
                            <input type='text' name="descripcion" value="{{$tipoSala -> descripcion}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="Editar" class="btn btn-success"/>
                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/tipoSalas"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>
            </tr>
        </table>
    </div>

@endsection