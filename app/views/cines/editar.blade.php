@extends('layout.Plantilla')
@section('titulo')
    {{"Editar Cine"}}
@endsection

@section('body')
    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Editar Cine</h3>
        <table class="table">
            <form action="/Cine/public/cines/edit" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="nombre">Nombre:</label>
                        </td>
                        <td>
                            <input type="hidden" name="id" value="{{$cine -> id}}">
                            <input type='text' name="nombre" value="{{$cine -> nombre}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="direccion">Direccion:</label>
                        </td>
                        <td>
                            <input type='text' name="direccion" value="{{$cine -> direccion}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="telefono">Telefono:</label>
                        </td>
                        <td>
                            <input type='number' name="telefono" value="{{$cine -> telefono}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="latitud">Latitud:</label>
                        </td>
                        <td>
                            <input type='text' name="latitud" value="{{$cine -> latitud}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="longitud">Longitud:</label>
                        </td>
                        <td>
                            <input type='text' name="longitud" value="{{$cine -> longitud}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="hora_apertura">Hora Apertura:</label>
                        </td>
                        <td>
                            <input type='text' name="hora_apertura" value="{{$cine -> hora_apertura}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="hora_cierre">Hora Cierre:</label>
                        </td>
                        <td>
                            <input type='text' name="hora_cierre" value="{{$cine -> hora_cierre}}">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="Editar" class="btn btn-success"/>

                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/cines"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>
            </tr>
        </table>
    </div>

    @endSection