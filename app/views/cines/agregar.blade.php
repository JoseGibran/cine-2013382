@extends('layout.Plantilla')
@section('titulo')
    {{"Agregar Cine"}}
@endsection

@section('body')
    <div class="col-md-3 "></div>
    <div class="col-md-6 ">
        <h3 class="panel panel-active">Nuevo Cine</h3>
        <table class="table">
            <form action="/Cine/public/cines/create" method='get'>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="nombre">Nombre:</label>
                        </td>
                        <td>
                            <input type='text' name="nombre">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="direccion">Direccion:</label>
                        </td>
                        <td>
                            <input type='text' name="direccion">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="telefono">Telefono:</label>
                        </td>
                        <td>
                            <input type='number' name="telefono">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="latitud">Latitud:</label>
                        </td>
                        <td>
                            <input type='text' name="latitud">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="longitud">Longitud:</label>
                        </td>
                        <td>
                            <input type='text' name="longitud">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="hora_apertura">Hora Apertura:</label>
                        </td>
                        <td>
                            <input type='text' name="hora_apertura">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>
                            <label for="hora_cierre">Hora Cierre:</label>
                        </td>
                        <td>
                            <input type='text' name="hora_cierre">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>

                        </td>
                        <td>
                            <input type='submit' id="submit" value="Agregar" class="btn btn-success"/>

                        </td>
                    </div>
                </tr>
            </form>
            <tr>
                <td>

                </td>
                <td>
                    <a href="/Cine/public/cines"><Button class="btn btn-warning">Cancelar</Button></a>
                </td>
            </tr>
        </table>
    </div>

    @endSection