@extends('layout.Plantilla')
@section('titulo')
    {{'Cines'}}
@endSection
@section('body')
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Direccion
                </th>
                <th>
                    Telefono
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($cines as $c){?>
                <tr>
                   <td>{{$c -> nombre;}}</td>
                    <td>{{$c -> direccion;}}</td>
                    <td>{{$c -> telefono;}}</td>
                    <td>
                        <a href="/Cine/public/detalleCine/{{$c -> id}}"><button class="btn btn-info">Detalles</button></a>
                        <a href="/Cine/public/cines/eliminar/{{$c -> id}}"><button class="btn btn-danger">Eliminar</button></a>
                        <a href="/Cine/public/cines/editar/{{$c -> id}}"><button class="btn btn-warning">Editar</button></a>
                    </td>
                </tr>
            <?php }?>
            </tbody>
        </table>
       <a href="/Cine/public/cines/agregar"><button class="btn btn-success">Agregar</button></a>
    </div>
@endsection