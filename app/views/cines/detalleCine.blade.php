@extends('layout.Plantilla')


@section('titulo')

    {{ $cine->nombre}}

@endSection

    <?php
        /*SELECT Pelicula.titulo , Cine.nombre FROM Cartelera
    INNER JOIN Pelicula ON Cartelera.pelicula_id = Pelicula.id
    INNER JOIN Sala ON Cartelera.sala_id = Sala.id
    INNER JOIN Cine ON Sala.cine_id = Cine.id GROUP BY Cine.nombre;*/

    ?>
@section('search')
    <form method="get" class="navbar-form navbar-left" action="/Cine/public/detalleCine/{{$cine->id}}">
        <div class="form-group">
            <input class="form-control" type="text" name="id">
        </div>
        <input type="submit" value="Buscar" class="btn"/>
    </form>
@endsection

@section('body')
    <div class="col-md-12">
        <center>
            <h2>{{$cine->nombre}}<small>detalles</small></h2>
            <h3>Direccion: {{$cine -> direccion}}</h3>
            <h3>Telefono: {{$cine -> telefono}}</h3>
            <h3>Horario: {{$cine ->hora_apertura}} ; {{$cine->hora_cierre}}</h3>
            <h4>Peliculas disponibles</h4>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>
                        Titulo
                    </th>
                    <th>
                        Genero
                    </th>
                    <th>
                        Clasificacion
                    </th>
                    <th>
                        Fecha
                    </th>
                </tr>
                </thead>
                <tbody>

        <?php
       $peliculas = DB::table('Cartelera')
        ->where('Cine.id', '=', $cine->id)
        ->join('Pelicula', 'Cartelera.pelicula_id', '=', 'Pelicula.id')
        ->join('Sala', 'Cartelera.sala_id', '=', 'Sala.id')
        ->join('Cine', 'Sala.cine_id', '=', 'Cine.id')
        ->select('Pelicula.id', 'Cartelera.fecha')
        ->get();
        foreach($peliculas as $p){
            $pelicula = Pelicula::find($p->id)
        ?>

            <tr>
                <td>{{$pelicula->titulo}}</td>
                <td>{{$pelicula->genero}}</td>
                <td>{{$pelicula->rated}}</td>
                <td>{{$p->fecha}}</td>
            </tr>

        <?php } ?>

             <tbody/>
             </table>
        </center>
    </div>

@endsection
