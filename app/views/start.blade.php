@extends('layout.Plantilla')
@section('titulo')
    {{'Cine-2013382'}}
@endsection
@section('css')
    <style>
        .thumbnail img{
            width: 200px;
            height: 300px;
        }
    </style>
@endsection
@section('body')
    <div class="col-md-12"></div>
        <?php foreach(Pelicula::all() as $p){ ?>
        <div class="col-md-3">

            <div class="thumbnail">
                <img alt="Bootstrap Thumbnail First" src="{{$p -> image}}" />
                <div class="caption">
                    <center>
                    <p>
                        <span class="label label-success">{{$p -> rated}}</span>
                        <span class="label label-primary">Duracion</span>
                        <span class="label label-danger">{{$p -> genero}}</span>
                    </p>

                    <p>
                        <a class="btn btn-primary" href="/Cine/public/peliculas/detalles/{{$p -> id}}">Mas</a>
                    </p></center>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
@endsection
@section('scripts')
    <script>

    </script>
@endsection
