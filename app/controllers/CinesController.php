<?php

class CinesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Cine::all();
	}
    public function getIndex()
    {
        $cines = Cine::all();
        return View::make('cines/cines', array('cines' => $cines));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$cine =  Cine::findOrFail($id);
        //return $cine -> salas -> first()->carteleras()->first();
        return View::make('cines/detalleCine', array('cine' => $cine));
	}

    public function showJson($id)
    {
        $cine = Cine::findOrFail($id);
        return  $peliculas = DB::table('Cartelera')
            ->where('Cine.id', '=', $cine->id)
            ->join('Pelicula', 'Cartelera.pelicula_id', '=', 'Pelicula.id')
            ->join('Sala', 'Cartelera.sala_id', '=', 'Sala.id')
            ->join('Cine', 'Sala.cine_id', '=', 'Cine.id')
            ->select('Pelicula.id', 'Cartelera.fecha')
            ->get();
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

    public function getCreate()
    {
        $cine = new Cine();
        $cine -> nombre = $_GET['nombre'];
        $cine -> direccion = $_GET['direccion'];
        $cine -> telefono = $_GET['telefono'];
        $cine -> latitud = $_GET['latitud'];
        $cine -> longitud = $_GET['longitud'];
        $cine -> hora_apertura = $_GET['hora_apertura'];
        $cine -> hora_cierre = $_GET['hora_cierre'];
        $cine -> save();
        return $this::getIndex();
    }
    public function getAgregar()
    {
        return View::make('cines/agregar');
    }

    public function getEliminar($id){
        $cine = Cine::find($id);
        $cine -> delete();
        return $this::getIndex();
    }

    public function getEditar($id){
        $cine = Cine::find($id);
        return View::make('cines/editar', array("cine" => $cine));
    }

	public function getEdit()
	{
        $cine = Cine::find($_GET['id']);
        $cine -> nombre = $_GET['nombre'];
        $cine -> direccion = $_GET['direccion'];
        $cine -> telefono = $_GET['telefono'];
        $cine -> latitud = $_GET['latitud'];
        $cine -> longitud = $_GET['longitud'];
        $cine -> hora_apertura = $_GET['hora_apertura'];
        $cine -> hora_cierre = $_GET['hora_cierre'];
        $cine -> save();
        return $this::getIndex();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
