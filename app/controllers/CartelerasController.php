<?php

class CartelerasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
    public function getIndex(){
        $carteleras = Cartelera::all();
        return View::make('carteleras/carteleras', array('carteleras' => $carteleras));
    }

    public function getAgregar(){
        Return View::make('carteleras/agregar');
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$cartelera = new Cartelera();
        $cartelera -> sala_id = $_GET['sala_id'];
        $cartelera -> pelicula_id = $_GET['pelicula_id'];
        $cartelera -> formatopelicula_id = $_GET['formatopelicula_id'];
        $cartelera -> formato_lenguaje =$_GET['formato_lenguaje'];
        $cartelera -> fecha = $_GET['fecha'];
        $cartelera -> hora = $_GET['hora'];
        $cartelera -> save();
        return $this::getIndex();
	}

    public function getEliminar($id){
        Cartelera::find($id) -> delete();
        return $this::getIndex();
    }
    public function getEditar($id){
        return View::make('carteleras/editar', array('cartelera' => Cartelera::find($id)));
    }
    public function getEdit(){
        $cartelera = Cartelera::find($_GET['id']);
        $cartelera -> sala_id = $_GET['sala_id'];
        $cartelera -> pelicula_id = $_GET['pelicula_id'];
        $cartelera -> formatopelicula_id = $_GET['formatopelicula_id'];
        $cartelera -> formato_lenguaje =$_GET['formato_lenguaje'];
        $cartelera -> fecha = $_GET['fecha'];
        $cartelera -> hora = $_GET['hora'];
        $cartelera -> save();
        return $this::getIndex();
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
