<?php

class SalasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$salas =  Sala::all();
        return View::make('salas/salas', array('salas' => $salas));
	}

    public function getAgregar()
    {
        return View::make('salas/agregar');
    }
    public function getCreate()
    {
        $sala = new Sala();
        $sala -> cine_id = $_GET['cine_id'];
        $sala -> tiposala_id = $_GET['tiposala_id'];
        $sala -> numero = $_GET['numero'];
        $sala ->save();
        return $this::getIndex();
    }
    public function getEliminar($id)
    {
        $sala = Sala::find($id);
        $sala -> delete();
        return $this -> getIndex();
    }

    public function getEditar($id)
    {
        $sala = Sala::find($id);
        return View::make('salas/editar', array('sala' => $sala));
    }
    public function getEdit(){
        $s = Sala::find($_GET['id']);
        $s -> cine_id = $_GET['cine_id'];
        $s -> tiposala_id = $_GET['tiposala_id'];
        $s -> numero = $_GET['numero'];
        $s ->save();
        return $this::getIndex();
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
