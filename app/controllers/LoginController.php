<?php

class LoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postUser(){
       $userdata = array(
           'userName' =>Input::get('userName'),
           'password' => Input::get('pass')
       );
       if(Auth::attempt($userdata))
       {
           return  View::make('start',  array('userName' => Input::get('userName')));
       }else{
           return View::make('login/login', array('error' => true));
       }
    }

    public function postRegistro(){
        $user = new User();
        $user -> userName = Input::get('userName');
        $user -> password = Hash::make(Input::get('password'));
        $user -> save();

        return  View::make('start',  array('userName' => Input::get('userName')));

    }


    public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
