<?php

class PeliculasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Pelicula::all();
	}

    public function getIndex(){
        $peliculas = Pelicula::all();
        return View::make('peliculas/peliculas', array('peliculas' =>  $peliculas, 'titulo' => 'Peliculas'));

    }

    public function getEstrenos(){
        $estrenos = DB::table('Estreno')
            ->join('Pelicula', 'Estreno.pelicula_id', '=', 'Pelicula.id')
            ->select('Pelicula.id','Pelicula.titulo','Pelicula.genero', 'Pelicula.rated')
            ->get();
        return View::make('peliculas/peliculas', array('peliculas' => $estrenos, 'titulo' => 'Estrenos'));
    }
    public function getEstrenosJson(){
        return $estrenos = DB::table('Estreno')
            ->join('Pelicula', 'Estreno.pelicula_id', '=', 'Pelicula.id')
            ->select('Pelicula.id','Pelicula.titulo','Pelicula.sinopsis','Pelicula.trailer_url','Pelicula.image','Pelicula.genero', 'Pelicula.rated')
            ->get();
    }
    public function getPreventasJson(){
        return $preventas = DB::table('PreVenta')
            ->join('Estreno', 'PreVenta.estreno_id', '=', 'Estreno.id')
            ->join('Pelicula', 'Estreno.pelicula_id', '=', 'Pelicula.id')
            ->select('Pelicula.id','Pelicula.titulo','Pelicula.sinopsis','Pelicula.trailer_url','Pelicula.image','Pelicula.genero', 'Pelicula.rated')
            ->get();
    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
    public function getAgregar(){
        return View::make('peliculas/agregar');
    }


	public function getCreate()
	{

        $pelicula = new Pelicula();
        $pelicula->titulo = $_GET['titulo'];
        $pelicula->sinopsis = $_GET['sinopsis'];
        $pelicula->trailer_url = $_GET['trailer_url'];
        $pelicula->image = $_GET['image'];
        $pelicula->rated = $_GET['rated'];
        $pelicula->genero = $_GET['genero'];
        $pelicula->save();
        if(isset($_GET['isEstreno'])){
            if($_GET['isEstreno'] == 'on'){
                $estreno = new Estreno();
                $estreno -> pelicula_id = $pelicula -> id;
                $estreno -> fecha_comienzo = $_GET['fecha_comienzo'];
                $estreno -> fecha_fin = $_GET['fecha_fin'];
                $estreno -> estado = true;
                $estreno -> save();
            }
        }

        if(isset($_GET['isPreVenta'])){
            if($_GET['isPreVenta'] == 'on'){
                $preVenta = new PreVenta();
                $preVenta -> estreno_id = $estreno -> id;
                $preVenta -> cine_id = $_GET['cine_id'];
                $preVenta -> save();
            }
        }

        return $this::getIndex();
	}

    public function getDelete($id){
        $estreno = DB::table('Estreno')
            ->where('pelicula_id', '=', $id)
            ->select('Estreno.id');
        if(count($estreno->get()) > 0){
            $preVenta = DB::table('PreVenta')
                ->where('estreno_id', '=', $estreno -> first() -> id)
                ->select('PreVenta.id');
            if(count($preVenta->get())>0){
                PreVenta::find($preVenta -> first() -> id)->delete();
            }
           Estreno::find($estreno -> first() -> id) ->delete();
        }
        $pelicula = Pelicula::find($id);
        $pelicula -> delete();
        return $this::getIndex();
    }

    public function getEditar($id){
        $pelicula = Pelicula::find($id);
        return View::make('peliculas/editar', array('pelicula' => $pelicula));
    }
    public function getUpdate(){
        $pelicula = Pelicula::find($_GET['id']);
        $pelicula->titulo = $_GET['titulo'];
        $pelicula->sinopsis = $_GET['sinopsis'];
        $pelicula->trailer_url = $_GET['trailer_url'];
        $pelicula->image = $_GET['image'];
        $pelicula->rated = $_GET['rated'];
        $pelicula->genero = $_GET['genero'];
        $pelicula->save();
        return $this::getIndex();
    }

    public function getDetalles($id){
        $pelicula = Pelicula::find($id);
        return View::make('peliculas/detalles', array('pelicula' => $pelicula));
    }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
