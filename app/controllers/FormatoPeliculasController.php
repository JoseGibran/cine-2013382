<?php
/**
 * Created by PhpStorm.
 * User: gibran
 * Date: 1/08/15
 * Time: 07:23 AM
 */

class FormatoPeliculasController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $formatoPeliculas = FormatoPelicula::all();
        return View::make('formatoPeliculas/formatoPeliculas', array('formatoPeliculas' => $formatoPeliculas));
    }


    public function getAgregar(){
        return View::make('formatoPeliculas/agregar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $formatoPelicula = new FormatoPelicula();
        $formatoPelicula -> nombre  = $_GET['nombre'];
        $formatoPelicula -> descripcion = $_GET['descripcion'];
        $formatoPelicula -> save();
        return $this::getIndex();
    }

    public function getEliminar($id){
        FormatoPelicula::find($id) -> delete();
        return $this::getIndex();
    }
    public function getEditar($id){
        $formatoPelicula = FormatoPelicula::find($id);
        return View::make('formatoPeliculas/editar', array('formatoPelicula' => $formatoPelicula));
    }
    public function getEdit(){
        $formatoPelicula = FormatoPelicula::find($_GET['id']);
        $formatoPelicula -> nombre  = $_GET['nombre'];
        $formatoPelicula -> descripcion = $_GET['descripcion'];
        $formatoPelicula -> save();
        return $this::getIndex();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}